#!/bin/env python3

import catt.api as capi
import threading 

video = input("please provide URL: ") 


devices = capi.discover()

def getname(name):
	name = str(name)
	name = name.replace("<CattDevice: ",'')
	name = name.replace(">",'')
	return name


class Caster (threading.Thread):
    def __init__(self,video,dev):
        self.__video = video
        self.__dev = dev
        threading.Thread.__init__(self)
    def run (self):
          cast = capi.CattDevice(name=str(self.__dev))
          cast.play_url(self.__video, resolve=True, block=True)


for device in devices:
    device = getname(device)
    print("Terrorizing " + device + " >:I")
    # cast.play_url(video, resolve=True, block=True)
    Caster(video,device).start()
    print("video started on: " + device)
print("Enjoy the chaos :D")
